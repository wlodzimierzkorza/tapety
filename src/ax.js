import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://pantry-1ec71.firebaseio.com/',
});

export default instance;
