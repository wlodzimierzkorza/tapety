import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Fade } from 'reactstrap';
import withURL from 'hoc/withURL';
import styled from 'styled-components';
import { getResults, getFavorites } from 'actions/actions';
import ImageWrapper from 'Components/Image/ImageWrapper';
import Image from 'Components/Image/Image';
import ImageData from 'Components/Image/ImageData';
import Pagination from 'Components/Pagination';
import Search from 'Components/Search/index';

const StyledContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`;

const RootView = ({ url, results, getResults, queries, getFavorites }) => {
  useEffect(() => {
    getResults(url);
    getFavorites('/favorites.json');
  }, [url]);
  return (
    <Fade>
      <Search queries={queries} />
      <StyledContainer>
        {results &&
          results.map(item => {
            return (
              <ImageWrapper key={item.id} src={item.urls.small}>
                <Image src={item.urls.small} title={item.title} />
                <ImageData item={item} />
              </ImageWrapper>
            );
          })}
        <Pagination />
      </StyledContainer>
    </Fade>
  );
};

const mapStateToProps = state => {
  return {
    results: state.results,
    queries: state.queries,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getResults: url => {
      dispatch(getResults(url));
    },
    getFavorites: path => {
      dispatch(getFavorites(path));
    },
  };
};
export default withURL(connect(mapStateToProps, mapDispatchToProps)(RootView));
