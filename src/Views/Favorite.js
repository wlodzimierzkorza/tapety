import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Fade } from 'reactstrap';
import styled from 'styled-components';
import { getFavorites } from 'actions/actions';
import ImageData from 'Components/Image/ImageData';
import ImageWrapper from 'Components/Image/ImageWrapper';
import Image from 'Components/Image/Image';

const StyledContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`;

const Favorite = ({ favorites, getFavorites }) => {
  useEffect(() => {
    getFavorites('/favorites.json');
  }, []);
  return (
    <Fade>
      <StyledContainer>
        {favorites &&
          favorites.map(item => {
            return (
              <ImageWrapper key={item.id} src={item.urls.small}>
                <Image src={item.urls.small} title={item.title} />
                <ImageData item={item} />
              </ImageWrapper>
            );
          })}
      </StyledContainer>
    </Fade>
  );
};
const mapStateToProps = state => {
  return { favorites: state.favorites };
};
const mapDispatchToProps = dispatch => {
  return {
    getFavorites: path => {
      dispatch(getFavorites(path));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Favorite);
