import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const StyledHeader = styled.header`
  padding: 30px;
  margin-bottom: 30px;
  display: flex;
  align-content: center;
  box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.4);
`;

const StyledLink = styled.div`
  padding: 5px 15px;
`;

const Header = () => {
  return (
    <StyledHeader>
      <StyledLink>
        <Link to="/">Stona główna</Link>
      </StyledLink>
      <StyledLink>
        <Link to="/favorites">Ulubione</Link>
      </StyledLink>
    </StyledHeader>
  );
};

export default Header;
