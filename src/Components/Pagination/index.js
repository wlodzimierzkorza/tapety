import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { changePage } from 'actions/actions';

const StyledContainer = styled.div`
  width: 70%;
  margin: 30px 0 60px 15%;
  display: flex;
  justify-content: center;
`;

const StyledButton = styled.button`
  width: 60px;
  height: 60px;
  display: glex;
  margin: 0px 5px;
  border-radius: 5px;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.4);
  justify-content: center;
  align-items: center;
`;

const Pagination = ({ changePage, currentPage }) => {
  const prevPageHandler = () => {
    changePage(currentPage - 1);
  };

  const nextPageHandler = () => {
    changePage(currentPage + 1);
  };

  return (
    <StyledContainer>
      <StyledButton onClick={prevPageHandler}>Prev</StyledButton>
      <StyledButton onClick={nextPageHandler}>Next</StyledButton>
    </StyledContainer>
  );
};
const mapStateToProps = state => {
  return {
    currentPage: state.currentPage,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changePage: page => {
      dispatch(changePage(page));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Pagination);
