import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { updateQueries } from 'actions/actions';

const StyledInput = styled.input`
  background: none;
  border: none;
  width: 80%;
  font-weight: 900;
  margin-left: 10px;
  padding: 5px;
`;

const StyledSearch = styled.div`
  width: 80vw;
  margin-left: 10vw;
  border-radius: 20px;
  background: #f1f1f1;
  padding: 20px 60px;
  font-size: 20px;
  display: flex;
  color: #909090;
  span {
    padding-top: 5px;
    width: 200px;
  }
`;

const StyledButton = styled.button`
  border: none;
  border-radius: 10px;
  background: none;
  padding: 5px 20px;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.4);
`;

const Search = ({ queries, updateQueries }) => {
  const [query, setQuery] = useState('');
  useEffect(() => {
    setQuery(queries.join(',').toUpperCase());
  }, []);
  const changeQueryHandler = e => {
    setQuery(e.target.value);
  };
  const updateQueriesHandler = () => {
    const string = query.split(',');
    updateQueries(string);
  };

  return (
    <StyledSearch>
      <span>Słowa kluczowe: </span>
      <StyledInput type="text" value={query} onChange={changeQueryHandler} />
      <StyledButton onClick={updateQueriesHandler}>Szukaj</StyledButton>
    </StyledSearch>
  );
};

const mapStateToProps = state => {
  return {
    queries: state.queries,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateQueries: query => {
      dispatch(updateQueries(query));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Search);
