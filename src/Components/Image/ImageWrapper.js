import React from 'react';
import styled from 'styled-components';

const StyledImageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 48vw;
  overflow: hidden;
  position: relative;
  padding: 60px;
  width: 48vw;
  margin: 1vw;
  border: solid 4px #fff;
  overflow: hidden;
`;
const StyledBlurWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-image: url(${({ src }) => src});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  filter: blur(4px);
  opacity: 0.5;
`;

const ImageWrapper = ({ children, src }) => {
  return (
    <StyledImageWrapper>
      <StyledBlurWrapper src={src} />
      {children}
    </StyledImageWrapper>
  );
};

export default ImageWrapper;
