import React from 'react';
import styled from 'styled-components';

const StyledThumb = styled.div`
  padding: 5px;
  z-index: 10;
  background: #fff;
  box-shadow: 0 10px 15px 0 rgba(0, 0, 0, 0.5);
`;

const StyledImage = styled.img`
  width: 100%;
  height: 100%;
`;
const Image = ({ src, title }) => {
  return (
    <StyledThumb>
      <StyledImage src={src} alt={title} />
    </StyledThumb>
  );
};

export default Image;
