import React from 'react';
import { connect } from 'react-redux';
import { deleteFromFavorites, addToFavorites } from 'actions/actions';
import styled from 'styled-components';
import heart from 'addons/img/heart.png';

const StyledData = styled.div`
  width: 80%;
  background: #fff;
  position: absolute;
  bottom: 25px;
  border-radius: 5px;
  height: 60px;
  left: 10%;
  padding: 20px;
  z-index: 100;
  display: flex;
  justify-content: space-between;
  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.6);
`;
const StyledButton = styled.button`
  border: none;
  padding: 0;
  width: 30px;
  height: 30px;
`;

const ImageData = ({
  addToFavorites,
  deleteFromFavorites,
  favorites,
  item,
}) => {
  const { user, id, myID } = item;
  const addToFavoritesHandler = () => {
    const array = [...favorites];
    array.push(item);
    addToFavorites(item, array);
  };
  let check = false;
  if (favorites) {
    check = favorites.some(item => {
      return item.id === id;
    });
  }
  const deleteFromFavoritesHandler = myID => {
    const array = favorites.filter(item => {
      return item.myID !== myID;
    });
    deleteFromFavorites(array, myID);
  };

  const Icon =
    check === false ? (
      <StyledButton type="button" onClick={addToFavoritesHandler}>
        <img src={heart} alt="heart" height="100%" />
      </StyledButton>
    ) : (
      <StyledButton onClick={() => deleteFromFavoritesHandler(myID)}>
        X
      </StyledButton>
    );
  return (
    <StyledData>
      <span>Autor: {`${user.first_name} ${user.last_name}`}</span>
      <span>{Icon}</span>
    </StyledData>
  );
};

const mapStateToProps = state => {
  return {
    favorites: state.favorites,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteFromFavorites: (array, id) => {
      dispatch(deleteFromFavorites(array, id));
    },
    addToFavorites: (item, array) => {
      dispatch(addToFavorites(item, array));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageData);
