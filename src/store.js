import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducer from './reducer/reducer';
import { defaultQueries } from './utils';

const initialState = {
  currentPage: 1,
  queries: defaultQueries(),
  favorites: [],
};
const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(thunk)),
);

export default store;
