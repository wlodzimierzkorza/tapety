export const defaultQueries = () => {
  // const day = new Date().getDay();
  const hour = new Date().getHours();
  const month = new Date().getMonth();
  const query = {
    season: '',
    time: '',
  };

  if (month >= 1 && month < 3) {
    query.season = 'winter';
  } else if (month >= 3 && month < 5) {
    query.season = 'spring';
  } else if (month >= 5 && month < 9) {
    query.season = 'summer';
  } else {
    query.season = 'autumn';
  }

  if (hour < 12 && hour > 4) {
    query.time = 'morning';
  } else if (hour > 12 && hour < 19) {
    query.time = 'afternoon';
  } else if (hour > 19 && hour < 23) {
    query.time = 'evening';
  } else {
    query.time = 'night';
  }

  // switch (day) {
  //   case 1:
  //     query.day = 'monday';
  //     break;
  //   case 2:
  //     query.day = 'tuesday';
  //     break;
  //   case 3:
  //     query.day = 'wednesday';
  //     break;
  //   case 4:
  //     query.day = 'thursday';
  //     break;
  //   case 5:
  //     query.day = 'friday';
  //     break;
  //   case 6:
  //     query.day = 'saturday';
  //     break;
  //   case 7:
  //     query.day = 'sunday';
  //     break;
  //   default:
  //     return '';
  // }

  const queryArray = [];
  Object.keys(query).forEach(key => {
    queryArray.push(query[key]);
  });

  return queryArray;
};
