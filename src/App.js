/* eslint-disable no-unused-expressions */
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import Root from 'Views/Root';
import Favorite from 'Views/Favorite';
import Header from 'Components/Header';

const App = ({ queries, page }) => {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/">
          <Root queries={queries} page={page} />
        </Route>
        <Route path="/favorites">
          <Favorite />
        </Route>
      </Switch>
    </>
  );
};

const mapStateToProps = state => {
  return {
    queries: state.queries,
    page: state.currentPage,
  };
};

export default connect(mapStateToProps)(App);
