export default (state, { type, payload }) => {
  switch (type) {
    case 'GET_FAVORITES':
      return {
        ...state,
        favorites: payload,
      };
    case 'GET_RESULTS':
      return {
        ...state,
        results: payload.results,
      };
    case 'ADD_TO_FAVORITES':
      return {
        ...state,
        favorites: payload,
      };
    case 'UPDATE_QUERY':
      return {
        ...state,
        queries: payload,
      };
    case 'CHANGE_PAGE':
      return {
        ...state,
        currentPage: payload,
      };
    default:
      return state;
  }
};
