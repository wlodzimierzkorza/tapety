import React from 'react';

const withURL = WrappedComponent => {
  return props => {
    const { type, queries, page } = props;
    const createLink = (t, q, p) => {
      const query = `query=${q.join('%20')}`;
      const type = t ? `${t}/` : 'photos';
      const page = `page=${p}`;
      const link = `https://api.unsplash.com/search/${type}?${page}&per_page=10&${query}?&client_id=UNvP25-Hb6rK5S9P1hcBEvRVAXGLRDeeoF0-iP0871Q`;
      return link;
    };

    return (
      <WrappedComponent
        url={createLink(type, queries || [], page)}
        {...props}
      />
    );
  };
};

export default withURL;
