import ax from 'axios';
import axios from '../ax';

export const getFavorites = path => dispatch => {
  return axios.get(path).then(res => {
    const array = [];
    if (res.data) {
      Object.keys(res.data).forEach(key => {
        res.data[key].myID = key;
        array.push(res.data[key]);
      });
      dispatch({
        type: 'GET_FAVORITES',
        payload: array,
      });
    }
  });
};

export const getResults = url => dispatch => {
  return ax.get(url).then(res => {
    dispatch({
      type: 'GET_RESULTS',
      payload: res.data,
    });
  });
};
export const addToFavorites = (item, array) => dispatch => {
  return axios.post('favorites.json', item).then(() => {
    dispatch({
      type: 'ADD_TO_FAVORITES',
      payload: array,
    });
  });
};

export const deleteFromFavorites = (array, myID) => dispatch => {
  return axios.delete(`/favorites/${myID}.json`).then(() => {
    dispatch({
      type: 'ADD_TO_FAVORITES',
      payload: array,
    });
  });
};

export const updateQueries = query => dispatch => {
  return dispatch({
    type: 'UPDATE_QUERY',
    payload: query,
  });
};

export const changePage = page => dispatch => {
  return dispatch({
    type: 'CHANGE_PAGE',
    payload: page,
  });
};
